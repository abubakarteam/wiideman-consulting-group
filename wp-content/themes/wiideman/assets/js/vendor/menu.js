(function ($) {
  $.fn.menumaker = function (options) {
    var cssmenu = $(this), settings = $.extend({
      format: "dropdown",
      sticky: false
    }, options);
    return this.each(function () {
      $(this).find(".mobile-button").on('click', function () {
        $(this).toggleClass('menu-opened');
        var mainmenu = $(this).next('.mobile-menu-c');
        if (mainmenu.hasClass('open')) {
          mainmenu.slideToggle().removeClass('open');
        }
        else {
          mainmenu.slideToggle().addClass('open');
          if (settings.format === "dropdown") {
            mainmenu.find('.mobile-menu-c').show();
          }
        }
      });
      cssmenu.find('li ul').parent().addClass('has-sub');
      multiTg = function () {
        cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
        cssmenu.find('.submenu-button').on('click', function () {

          if ($(this).hasClass('submenu-opened')) {
            $('.submenu-button').removeClass('submenu-opened');
          }
          else {
            $('.submenu-button').removeClass('submenu-opened');
            $(this).addClass('submenu-opened');
          }

          if ($(this).siblings('ul').hasClass('open')) {
            $('.submenu-button').siblings('ul').removeClass('open').slideUp();
          }
          else {
            $('.submenu-button').siblings('ul').removeClass('open').slideUp();
            $(this).siblings('ul').addClass('open').slideDown();
          }
        });
      };
      if (settings.format === 'multitoggle') multiTg();
      else cssmenu.addClass('dropdown');
      if (settings.sticky === true) cssmenu.css('position', 'fixed');
      resizeFix = function () {
        var mediasize = 1199;
        if ($(window).width() > mediasize) {
          cssmenu.find('.mobile-menu-c').show();
        }
        if ($(window).width() <= mediasize) {
          cssmenu.find('.mobile-menu-c').hide().removeClass('open');
        }
      };
      resizeFix();
      return $(window).on('resize', resizeFix);
    });
  };
})(jQuery);

(function ($) {
  $(document).ready(function () {
    $("#cssmenu").menumaker({
      format: "multitoggle"
    });
  });
  $('.mobile-menu-c ul li a').click(function () {
    $('.mobile-menu-c').slideUp().removeClass('open');
    $('.mobile-button').removeClass('menu-opened');

  });
})(jQuery);
