/**
 *  
 * Custom File for Site Scripts
 */
jQuery(document).ready(function ($) {
	$.noConflict();
	$(document).on("scroll", function () {
		if
			($(document).scrollTop() > 0) {
			$("header").addClass("shrink");
		}
		else {
			$("header").removeClass("shrink");
		}
	});
	$.noConflict();
	// Testimonial Slider
	$(".testimonial-slider").owlCarousel({
		loop: true,
		margin: 0,
		dots: true,
		nav: true,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 1
			},
			1000: {
				items: 1
			}
		}
	});
	$.noConflict();

	// var $container = $('.cards-container');
	// $container.isotope({
	// 	filter: '*',
	// 	animationOptions: {
	// 		duration: 750,
	// 		easing: 'linear',
	// 		queue: false
	// 	}
	// });
	$('.posts-filter a').click(function () {
		$('.posts-filter .current').removeClass('current');
		$(this).addClass('current');
		var selector = $(this).attr('data-filter');
		$container.isotope({
			filter: selector,
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			}
		});
		return false;
	});
	var $container = $('.isotope');
	// only opacity for reveal/hide transition
	$container.isotope({})

	$('#filter-select').change(function () {
		$container.isotope({
			filter: this.value
		});
	});
	$.noConflict();
	// Fix SIdebar Image 
	jQuery("#right")
		.stickOnScroll({
			topOffset: $("#menu").outerHeight(),
			footerElement: $("#bottom"),
			bottomOffset: 130
		});
	$.noConflict();
	// Fix SIdebar Image 
	jQuery("#download-form")
		.stickOnScroll({
			topOffset: $("#menu").outerHeight(),
			footerElement: $("#bottom"),
			bottomOffset: 130
		});


	var container = document.querySelector('#ms-container');
	var msnry = new Masonry(container, {
		itemSelector: '.ms-item',
		columnWidth: '.ms-item',
	});

	$(".search-icon img").click(function () {
		$(".header-search-form").addClass('popup-opened');
	});
	$(".popup-cls").click(function () {
		$(".header-search-form").removeClass('popup-opened');
	});
});
