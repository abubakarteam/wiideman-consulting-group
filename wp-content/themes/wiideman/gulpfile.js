/**
 * Gulpfile.
 *
 * Gulp with WordPress.
 *
 * Implements:
 *      1. Live reloads browser with BrowserSync.
 *      2. CSS: Sass to CSS conversion, error catching, Autoprefixing, Sourcemaps,
 *         CSS minification, and Merge Media Queries.
 *      3. JS: Concatenates & uglifies Vendor and Custom JS files.
 *      4. Images: Minifies PNG, JPEG, GIF and SVG images.
 *      5. Watches files for changes in CSS or JS.
 *      6. Watches files for changes in PHP.
 *      7. Corrects the line endings.
 *      8. InjectCSS instead of browser page reload.
 *      9. Generates .pot file for i18n and l10n.
 *
 */
/**
 * Load WPGulp Configuration.
 *
 * TO DO: Customize your project in the wpgulp.js file.
 */
const config = require('./wpgulp.config.js'); //Changed for OurWpGulp
var includePathVar = [
    './assets/sass',
    config.bowerDir + 'bootstrap-sass/assets/stylesheets',
    config.bowerDir + 'font-awesome/scss',
    config.bowerDir + 'owl.carousel/src/scss',
];
/**
 * Load Plugins.
 *
 * Load gulp plugins and passing them semantic names.
 */
const gulp = require('gulp'); // Gulp of-course.
var gutil = require('gutil');
// CSS related plugins.
const sass = require('gulp-sass'); // Gulp plugin for Sass compilation.
const minifycss = require('gulp-uglifycss'); // Minifies CSS files.
const autoprefixer = require('gulp-autoprefixer'); // Autoprefixing magic.
const mmq = require('gulp-merge-media-queries'); // Combine matching media queries into one.
// JS related plugins.
const concat = require('gulp-concat'); // Concatenates JS files.
const uglify = require('gulp-uglify'); // Minifies JS files.
// Image related plugins.
const imagemin = require('gulp-imagemin'); // Minify PNG, JPEG, GIF and SVG images with imagemin.
// Utility related plugins.
const rename = require('gulp-rename'); // Renames files E.g. style.css -> style.min.css.
const lineec = require('gulp-line-ending-corrector'); // Consistent Line Endings for non UNIX systems. Gulp Plugin for Line Ending Corrector (A utility that makes sure your files have consistent line endings).
const filter = require('gulp-filter'); // Enables you to work on a subset of the original files by filtering them using a glob.
const sourcemaps = require('gulp-sourcemaps'); // Maps code in a compressed file (E.g. style.css) back to its original position in a source file (E.g. structure.scss, which was later combined with other css files to generate style.css).
const notify = require('gulp-notify'); // Sends message notification to you.
const browserSync = require('browser-sync').create(); // Reloads browser and injects CSS. Time-saving synchronized browser testing.
const cache = require('gulp-cache'); // Cache files in stream for later use.
const remember = require('gulp-remember'); //  Adds all the files it has ever seen back into the stream.
const plumber = require('gulp-plumber'); // Prevent pipe breaking caused by errors from gulp plugins.
const beep = require('beepbeep');
const count = require('gulp-count');
const removeEmptyLine = require('gulp-remove-empty-lines');
const bower = require('gulp-bower');
const jsBeautify = require('gulp-jsbeautifier');
const mainBowerFiles = require('main-bower-files');
const zip = require('gulp-zip');
const rm = require('gulp-rm');
const rimraf = require('gulp-rimraf');
//const wpPot = require( 'gulp-wp-pot' ); // For generating the .pot file.
//const sort = require( 'gulp-sort' ); // Recommended to prevent unnecessary changes in pot-file.
//const rtlcss = require( 'gulp-rtlcss' ); // Generates RTL stylesheet. //Changed for OurWpGulp
/**
 * Custom Error Handler.
 *
 * @param Mixed err
 */
const errorHandler = r => {
    notify.onError('\n\n❌  ===> ERROR: <%= error.message %>\n')(r); // Message to be displayed when error function is called
    beep(2); // Beep two times on error
    // this.emit('end');
};
/**
 * Task: `browser-sync`.
 *
 * Live Reloads, CSS injections, Localhost tunneling.
 * @link http://www.browsersync.io/docs/options/
 *
 * @param {Mixed} done Done.
 */
const browsersync = done => {
    browserSync.init({
        proxy: config.projectURL,
        open: config.browserAutoOpen,
        injectChanges: config.injectChanges,
        watchEvents: ['change', 'add', 'unlink', 'addDir', 'unlinkDir']
    });
    done();
};
// Helper function to allow browser reload with Gulp 4.
const reload = done => {
    browserSync.reload(); // Reload browser
    done();
};
/**
 * Task: `styles`.
 *
 * Compiles Sass, Autoprefixes it and Minifies CSS.
 *
 * This task does the following:
 *    1. Gets the source scss file
 *    2. Compiles Sass to CSS
 *    3. Writes Sourcemaps for it
 *    4. Autoprefixes it and generates style.css
 *    5. Renames the CSS file with suffix .min.css
 *    6. Minifies the CSS file and generates style.min.css
 *    7. Injects CSS or reloads the browser via browserSync
 */
gulp.task('styles', () => {
    return gulp
        .src(config.styleSRC, {
            allowEmpty: false // If main.scss not found - then it gives error because empty file is not permitted
        })
        .pipe(plumber(errorHandler)) // to explain error with better comment
        .pipe(sourcemaps.init()) // Save all maps to all a folder
        .pipe(
            sass({
                errLogToConsole: config.errLogToConsole,
                outputStyle: config.outputStyle,
                precision: config.precision,
                // outputStyle: 'compressed',
                sourceMap: true,
                includePaths: config.includePathVar,
            })
        )
        .on('error', sass.logError)
        .pipe(sourcemaps.write({ // Write sourcemaps back to main.css output file ( or bundle.css in our case)
            includeContent: false
        }))
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        // .pipe(autoprefixer(config.BROWSERS_LIST)) // Add browser related autoprefixing codes
        .pipe(sourcemaps.write('./'))
        // .pipe(sourcemaps.write())
        .pipe(lineec()) // Consistent Line Endings for non UNIX systems.
        .pipe(rename(config.cssBundleFilename + '.css'))
        .pipe(gulp.dest(config.styleDestination))
        .pipe(filter('**/*.css')) // Filtering stream to only css files.
        .pipe(mmq({ // Merge Media Queries
            log: false
        })) // Merge Media Queries only for .min.css version.
        .pipe(browserSync.stream()) // Reloads style.css if that is enqueued.
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(minifycss({
            maxLineLen: 10
        }))
        .pipe(lineec()) // Consistent Line Endings for non UNIX systems.
        .pipe(gulp.dest(config.styleDestination))
        .pipe(filter('**/*.css')) // Filtering stream to only css files.
        .pipe(browserSync.stream()) // Reloads style.min.css if that is enqueued.
        .pipe(notify({
            message: '\n\n✅  ===> STYLES — completed!\n',
            onLast: true
        }));
});
/**
 * Task: `Combine JS Files`.
 *
 * Concatenate and uglify all JS scripts.
 *
 * This task does the following:
 *     1. Gets the source folder for JS all files
 *     2. Concatenates all the files and generates bundle.js
 *     3. Renames the JS file with suffix .min.js
 *     4. Uglifes/Minifies the JS file and generates bundle.min.js
 */
gulp.task('scripts', () => {
    return gulp
        .src(config.jsSRC, {
            // since: gulp.lastRun('scripts')  // run task only on changed files 
        }) // Only run on changed files.
        .pipe(count('## number JS files selected')) // Count number of files to work with
        .pipe(plumber(errorHandler))
        .pipe(remember(config.jsVendorSRC)) // Bring all files back to stream.
        .pipe(concat(config.jsFile + '.js', {
            newLine: ';'
        }))
        .pipe(lineec()) // Consistent Line Endings for non UNIX systems.
        .pipe(gulp.dest(config.jsDestination))
        .pipe(
            rename({
                basename: config.jsFile,
                suffix: '.min'
            })
        )
        .pipe(uglify())
        .pipe(lineec()) // Consistent Line Endings for non UNIX systems.
        .pipe(gulp.dest(config.jsDestination))
        .pipe(notify({
            message: '\n\n✅  ===> JS Processes — completed!\n',
            onLast: true
        }));
});
/**
 * Task: `images`.
 *
 * Minifies PNG, JPEG, GIF and SVG images.
 *
 * This task does the following:
 *     1. Gets the source of images raw folder
 *     2. Minifies PNG, JPEG, GIF and SVG images
 *     3. Generates and saves the optimized images
 *
 * This task will run only once, if you want to run it
 * again, do it with the command `gulp images`.
 *
 * Read the following to change these options.
 * @link https://github.com/sindresorhus/gulp-imagemin
 */
gulp.task('images', () => {
    return gulp
        .src(config.imgSRC)
        .pipe(
            cache(
                imagemin([
                    imagemin.gifsicle({
                        interlaced: true
                    }),
                    imagemin.jpegtran({
                        progressive: true
                    }),
                    imagemin.optipng({
                        optimizationLevel: 3
                    }), // 0-7 low-high.
                    imagemin.svgo({
                        plugins: [{
                            removeViewBox: true
                        }, {
                            cleanupIDs: false
                        }]
                    })
                ])
            )
        )
        .pipe(gulp.dest(config.imgDST))
        .pipe(notify({
            message: '\n\n✅  ===> IMAGES — completed!\n',
            onLast: true
        }));
});
/**
 * Task: `clear-images-cache`.
 *
 * Deletes the images cache. By running the next "images" task,
 * each image will be regenerated.
 */
gulp.task('clearCache', function (done) {
    return cache.clearAll(done);
});
// Beautify HTML files - Source and Destination places are same 
gulp.task('beautify_html', function () {
    return gulp.src(config.beautiyHtmlPath, {
        base: './'
    }) //, { since: gulp.lastRun('beautify_html') })
        //    .pipe( remember( 'assets/clean/**/*' ) ) // Bring all files back to stream.
        .pipe(count('## HTML files[s] selected to be beautified'))
        .pipe(jsBeautify({
            indent_char: '\t',
            indent_size: 1
        }))
        .pipe(removeEmptyLine())
        .pipe(jsBeautify.reporter())
        .pipe(notify({
            message: '\n\n✅  ===> HTML Beautification — completed!\n',
            onLast: true
        }))
        .pipe(gulp.dest('./'))
        .pipe(count('Beautification of ## HTML files completed'));
});
// Beautify CSS files - Source and Destination places are same 
gulp.task('beautify_css', function () {
    return gulp.src(config.beautiyCssPath, {
        base: './'
    })
        .pipe(count('## CSS files[s] selected to be beautified'))
        .pipe(jsBeautify())
        .pipe(removeEmptyLine())
        .pipe(jsBeautify.reporter())
        .pipe(notify({
            message: '\n\n✅  ===> CSS Beautification — completed!\n',
            onLast: true
        }))
        .pipe(gulp.dest('./'))
        .pipe(count('Beautification of ## CSS files completed'));
});
// Beautify SCSS files - Source and Destination places are same 
gulp.task('beautify_scss', function () {
    return gulp.src(config.beautiyScssPath, {
        base: './'
    })
        .pipe(count('## CSS files[s] selected to be beautified'))
        .pipe(jsBeautify())
        .pipe(removeEmptyLine())
        .pipe(jsBeautify.reporter())
        .pipe(notify({
            message: '\n\n✅  ===> SCSS Beautification — completed!\n',
            onLast: true
        }))
        .pipe(gulp.dest('./'))
        .pipe(count('Beautification of ## SCSS files completed'));;
});
// Beautify JS files - Source and Destination places are same 
gulp.task('beautify_js', function () {
    return gulp.src(config.beautiyJsPath, {
        base: './'
    })
        .pipe(count('## JS files[s] selected to be beautified'))
        .pipe(jsBeautify())
        .pipe(removeEmptyLine())
        .pipe(jsBeautify.reporter())
        .pipe(notify({
            message: '\n\n✅  ===> JS Beautification — completed!\n',
            onLast: true
        }))
        .pipe(gulp.dest('./'))
        .pipe(count('Beautification of ## JS files completed'));;
});
// Beautify PHP files - Source and Destination places are same 
gulp.task('beautify_php', function () {
    return gulp.src(config.beautiyPhpPath, {
        base: './'
    })
        .pipe(count('## PHP files[s] selected to be beautified'))
        //      .pipe(phpMinify())
        .pipe(notify({
            message: '\n\n✅  ===> PHP Beautification — completed!\n',
            onLast: true
        }))
        .pipe(gulp.dest('./'))
        .pipe(count('Beautification of ## PHP files completed'));
});
// Delete old files related to beautification backup 
gulp.task('beautify_delete_backup', function () {
    return gulp.src(config.beautifyZipBuild, {
        read: false,
        allowEmpty: true
    })
        //        .pipe(rm())
        .pipe(rimraf({
            force: true
        }))
});
// Backup new theme files before beautification process
gulp.task('beautify_backup', function () {
    return gulp
        .src(config.beautiyBuildInclude)
        .pipe(gulp.dest(config.beautifyBuild))
        .pipe(notify({
            message: '\n\n✅  ===> Before Beautify process all theme related files copied!\n',
            onLast: true
        }))
});
// Make zip file for all backup files in the backup of beautification folder 
gulp.task('beautify_zip', function () {
    return gulp.src(config.beautifyBuild + "**/*")
        .pipe(zip(config.projectName + "-" + config.projectVersion + '-before-beautification-backup.zip'))
        .pipe(gulp.dest(config.beautifyZipBuild))
        .pipe(notify({
            message: '\n\n✅  ===> Before Beautify ZIP completed!\n',
            onLast: true
        }))
});
// Task to beautify all files with a backup set of files and also a zip file
gulp.task('beautify', gulp.series('beautify_delete_backup', 'beautify_backup', 'beautify_zip', 'beautify_js', 'beautify_scss', 'beautify_php', 'beautify_css', 'beautify_html'), function () {
    return;
});
// Installs the bower files in bower_components folder
gulp.task('bower', function () {
    return bower()
        .pipe(gulp.dest(config.bowerDir))
        .pipe(notify({
            message: '\n\n✅  ===> Bower installations — completed!\n',
            onLast: true
        }));
});
/**
 * Build task that moves essential theme files for production-ready sites
 *
 * buildFiles copies all the files in buildInclude to build folder - check variable values at the top
 * buildImages copies all the images from img folder in assets while ignoring images inside raw folder if any
 */
gulp.task('zip', function () {
    return gulp.src(config.build + "/**/*")
        .pipe(zip(config.projectName + "-" + config.projectVersion + '.zip'))
        .pipe(gulp.dest(config.buildZip))
        .pipe(notify({
            message: 'Zip fie completion complete',
            onLast: true
        }))
});
// Backup all theme related files only in build folder
gulp.task('build', function () {
    return gulp.src(config.buildInclude)
        .pipe(gulp.dest(config.build))
        .pipe(notify({
            message: 'Copy from buildFiles complete',
            onLast: true
        }))
});
// Remove unnecassary files in theme folders e.g. _notes folder
gulp.task('clean', gulp.series('clearCache'), function () {
    return gulp.src(config.cleanFiles, {
        read: false,
        allowEmpty: true
    })
        //        .pipe(rm())
        .pipe(rimraf({
            force: true
        }))
        .pipe(notify({
            message: '\n\n✅  ===> Clean Function — completed!\n',
            onLast: true
        }))
});
// Task to be run as final to build zip file of theme assets and files
gulp.task(
    'final',
    gulp.series('clean', 'build', 'zip', function () { })
);
/**
 * Watch Tasks. 
 *
 * Watches for file changes and runs specific tasks.
 */
gulp.task(
    'default',
    gulp.series('styles', 'scripts', 'images', browsersync, () => {
        gulp.watch(config.watchPhp, reload); // Reload on PHP file changes.
        gulp.watch([config.watchStyles, '!assets/css/bundle.css', '!assets/css/bundle.min.css'], gulp.parallel('styles')); // Reload on SCSS file changes.
        gulp.watch([config.watchJs, '!assets/js/bundle.js', '!assets/js/bundle.min.js'], gulp.series('scripts', reload)); // Reload on customJS file changes.
        gulp.watch(config.imgSRC, gulp.series('images', reload)); // Reload on customJS file changes.
        gulp.watch(config.watchHtml, reload); // Reload on customJS file changes.
    })
);
